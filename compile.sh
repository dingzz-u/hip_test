#!/bin/sh

echo "source env.sh"
source ./env.sh

echo "cmake -S . -B build_hip -DUSE_HIP=ON -DCMAKE_C_COMPILER=hipcc -DCMAKE_CXX_COMPILER=hipcc"
cmake -S . -B build_hip -DUSE_HIP=ON -DCMAKE_C_COMPILER=hipcc -DCMAKE_CXX_COMPILER=hipcc

echo "cmake --build build_hip"
cmake --build build_hip

echo "sbatch cmake_run.sbatch"
sbatch run.sbatch
