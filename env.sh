#!/bin/sh

module purge

echo " module load compiler/devtoolset/7.3.1"
module load compiler/devtoolset/7.3.1

echo " module load compiler/rocm/3.9"
module load compiler/rocm/3.9

echo " module load mpi/hpcx/2.4.1/gcc-7.3.1-dcudirect"
module load mpi/hpcx/2.4.1/gcc-7.3.1-dcudirect

echo " module load compiler/cmake/3.15.6"
module load compiler/cmake/3.15.6

export MY_HIP_PATH=$HOME/dzz/software/rocm/rocm-3.9.0/hip/
export HIP_CMAKE_PATH=$HIP_PATH/lib/cmake/hip
export MY_HIP_CMAKE_PATH=$MY_HIP_PATH/lib/cmake/hip
