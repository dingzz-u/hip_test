#ifndef __TIMER_DEV_H__
#define __TIMER_DEV_H__

#ifndef __HIP_RUNTIME_H__
#define __HIP_RUNTIME_H__
#include "hip/hip_runtime.h"
#endif

#include <map>
#include <vector>


namespace antmoc {

namespace bak{

    class Timer_dev {

    private:

        std::map<hipStream_t, std::vector<hipEvent_t>> _start_times;

        float _elapsed_time;

        bool _running;

        /** A vector of the times and messages for each split */
        // static std::map<std::string, double> _timer_splits;
        std::map<const char*, float> _timer_splits;

        Timer_dev& operator=(const Timer_dev&) { return *this; }

        void destroyEvent();

    public:
        /**
         * @brief Constructor sets the current split elapsed time to zero.
         */
        Timer_dev() {
            _running = false;
            _elapsed_time = 0.0;
        }

        /**
         * @brief Destructor.
         */
        ~Timer_dev() { 
            destroyEvent();
        }

        /**
         * @brief Returns a static instance of the Timer class.
         * @return a pointer to the static Timer class
         */
        static Timer_dev* Get() {
            static Timer_dev instance;
            return &instance;
        }
        
        void startTimer(hipStream_t stream = (hipStream_t)0);
        void stopTimer(hipStream_t stream = (hipStream_t)0);
        float getTime();
        
        void stopTimer(const char* &msg, hipStream_t stream = (hipStream_t)0);
        void recordSplit(const char* &msg);
        void appendSplit(const char* &msg, float time);
        float getSplit(const char* &msg);

        void clearSplit(const char*& msg);
        void clearAllSplits();
        
        void printAllSplits();

    };

}
}

#endif
