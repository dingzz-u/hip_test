
#ifndef __HIP_RUNTIME_H__
#define __HIP_RUNTIME_H__
#include "hip/hip_runtime.h"
#endif

#include <stdio.h>
#include <unistd.h>

#include "src/log_dev.cpp"

#include "src/Timer_dev.cpp"

using namespace antmoc;


#define NUM 4

__global__ void callLogDev() {


//    const char* level_s = "info";
//    log_dev::set_level(level_s);  // 有这句的时候好像printf会乱？？它首字符会出现在别的行？？？是最后没加\n吗？？？？
    // set_level(level_s);
	// 直接在参数里写"info"会报错：candidate function not viable: no known conversion from 'const char [5]' to 'const char *&' for 1st argument


//    log_dev::info("Iteration %d:  k_eff = %1.6f   res = %1.3E  delta-k (pcm) = %d D.R. = %1.4f", 1, 0.698627, 0.08311, 3884, 0.8717);


	log_dev::info("=== test log ===");

    int int_1 = 5;
    int int_2 = 4;
    double d_1 = 88.888888;
    double d_2 = 444.44;

    char msg[] = "v_name";

    log_dev::info("this is %s", &msg);

    log_dev::info("n1 = %d, n2 = %d, f1 = %4f", (int*)int_1, (int*)int_2, &d_1);
	
	log_dev::debug("n1 = %d, f2 = %e, finish", (int*)int_2, &d_2);
	
	log_dev::verbose_once("test verbose_once, print f1 = %f", &d_2);

}



__global__ void printStream_1() {
    int i = 0;
    while (i < 100) {
//        printf("stream_1 is printing!");
        i++;
    }
	printf("\n");
}
__global__ void printStream_2() {
    int i = 0;
    while (i < 10000) {
//        printf("stream_2 is printing!!");
        i++;
    }
	printf("\n");
}
__global__ void printStream_3() {
    int i = 0;
    while (i < 500000) {
//        printf("stream_3 is printing!!!");
        i++;
    }
	printf("\n");
}




int main() {
    char* msg_buffer;
    
    hipDeviceProp_t devProp;
    hipGetDeviceProperties(&devProp, 0);

    printf("Device name in main : %s\n", devProp.name);

    /* start to test log_dev */
   
    log_dev::initialize_host();

    hipLaunchKernelGGL(callLogDev, dim3(1, 1, 1), dim3(NUM, 1, 1), 0, 0);
                            
    hipDeviceSynchronize();

    log_dev::destroy();
    
	
    /* start to test Timer_dev */

    const char* msg_1 = "this is stream_1";
    const char* msg_2 = "this is stream_2";
    const char* msg_3 = "this is stream_3";

    float time_s_1 = 0.0;
    float time_s_2 = 0.0;
    float time_s_3 = 0.0;

    Timer_dev* _dev_timer = new Timer_dev();

    hipStream_t stream1, stream2, stream3;
    hipStreamCreate(&stream1);
    hipStreamCreate(&stream2);
    hipStreamCreate(&stream3);

    _dev_timer->startTimer(stream1);
	hipLaunchKernelGGL(printStream_1, dim3(1, 1, 1), dim3(NUM, 1, 1), 0, stream1);
    _dev_timer->stopTimer(msg_1, stream1);
    time_s_1 = _dev_timer->getSplit(msg_1);


    _dev_timer->startTimer(stream2);
    hipLaunchKernelGGL(printStream_2, dim3(1, 1, 1), dim3(NUM, 1, 1), 0, stream2);
    _dev_timer->stopTimer(msg_2, stream2);
    time_s_2 = _dev_timer->getSplit(msg_2);


    _dev_timer->startTimer(stream3);
    hipLaunchKernelGGL(printStream_3, dim3(1, 1, 1), dim3(NUM, 1, 1), 0, stream3);
    _dev_timer->stopTimer(msg_3, stream3);
    time_s_3 = _dev_timer->getSplit(msg_3);

    hipDeviceSynchronize();

    printf("\n\n===time of stream_1 is : %f\n", time_s_1);
    printf("\n\n===time of stream_2 is : %f\n", time_s_2);
    printf("\n\n===time of stream_3 is : %f\n", time_s_3);

    _dev_timer->printAllSplits();

    delete _dev_timer;

    return 0;
}


