
#include "Timer_dev.h"

#ifndef __HIP_RUNTIME_H__
#define __HIP_RUNTIME_H__
#include "hip/hip_runtime.h"
#endif

#include <stdio.h>
#include <map>
#include <vector>

namespace antmoc {

class Timer_dev {

    private:
    
    std::map<hipStream_t, std::vector<hipEvent_t>> _start_times;

    float _elapsed_time;

    bool _running;

    /** A vector of the times and messages for each split */
    // static std::map<std::string, double> _timer_splits;
    std::map<const char*, float> _timer_splits;

    Timer_dev& operator=(const Timer_dev&) { return *this; }

    void destroyEvent(){
        hipEvent_t hipEvent = NULL;

        // clear all the hipEvent_t in each stream
        for (std::map<hipStream_t, std::vector<hipEvent_t>>::iterator iter = _start_times.begin(); iter != _start_times.end(); iter++) {
            while (!(iter->second).empty()) {
                hipEvent = (iter->second).back();
                hipEventDestroy(hipEvent);
                (iter->second).pop_back();
            }
        }

        // clear the map
        _start_times.clear();
    }
    
    public:
        /**
         * @brief Constructor sets the current split elapsed time to zero.
         */
        Timer_dev() {
            _running = false;
            _elapsed_time = 0.0;
        }

        /**
         * @brief Destructor.
         */
        ~Timer_dev() {
            destroyEvent();
        }

        /**
         * @brief Returns a static instance of the Timer class.
         * @return a pointer to the static Timer class
         */
        static Timer_dev* Get() {
            static Timer_dev instance;
            return &instance;
        }

    /**
     * @brief Starts the Timer.
     * @details This method is similar to starting a stopwatch.
     */
    void startTimer(hipStream_t stream = (hipStream_t)0) {

        hipError_t hipStatus;
        hipEvent_t hipEvent = NULL;
        
        hipStatus = hipEventCreate(&hipEvent);

        hipStatus = hipEventRecord(hipEvent, stream);
        if (hipStatus != hipSuccess) {
            fprintf(stderr, "hipEventRecord(start) failed!");
        }
        
        std::map<hipStream_t, std::vector<hipEvent_t>>::iterator iter = _start_times.find(stream);
        if (iter != _start_times.end()) { // exist
            (iter->second).push_back(hipEvent);
        }
        else {
            std::vector<hipEvent_t> event_vec;
            event_vec.push_back(hipEvent);
            _start_times.insert(std::map<hipStream_t, std::vector<hipEvent_t>>::value_type(stream, event_vec));
        }

        _running = true;

    Error:

        return;
    }


    // 设备端停止计时函数

    /**
     * @brief Stops the Timer.
     * @details This method is similar to stopping a stopwatch.
     */
    void stopTimer(hipStream_t stream = (hipStream_t)0) {
        if (_running) {
            hipError_t hipStatus;
            hipEvent_t hipEvent_stop = NULL;
            
            hipStatus = hipEventCreate(&hipEvent_stop);

            hipStatus = hipEventRecord(hipEvent_stop, stream);
            if (hipStatus != hipSuccess) {
                fprintf(stderr, "hipEventRecord(stop) failed!");
                goto Error;
            }
            
            // 同步的参数没有Stream？？？
            hipStatus = hipEventSynchronize(hipEvent_stop);
            if (hipStatus != hipSuccess) {
                fprintf(stderr, "hipEventSynchronize(stop) failed!");
                goto Error;
            }
            
            std::map<hipStream_t, std::vector<hipEvent_t>>::iterator iter = _start_times.find(stream);
            if (iter != _start_times.end()) { // exist
                hipEvent_t hipEvent_start = (iter->second).back();
                (iter->second).pop_back();

                if ((iter->second).empty()) {
                    _running = false;
                }

                hipStatus = hipEventElapsedTime(&_elapsed_time, hipEvent_start, hipEvent_stop);
                if (hipStatus != hipSuccess) {
                    fprintf(stderr, "hipEventElapsedTime(&_elapsed_time, start, stop) failed!");
                    goto Error;
                }
                _elapsed_time /= 1000.0;
            }
            else {
                fprintf(stderr, "there is no start_event recorded in this stream!");
            }

        }

    Error:

        return;
    }

	void stopTimer(const char*& msg, hipStream_t stream = (hipStream_t)0) {
        if (_running) {
            hipEvent_t hipEvent_stop = NULL;
            hipError_t hipStatus;

            hipStatus = hipEventCreate(&hipEvent_stop);

            hipStatus = hipEventRecord(hipEvent_stop, stream);
            if (hipStatus != hipSuccess) {
                fprintf(stderr, "hipEventRecord(stop, stream) failed!");
                goto Error;
            }

            // 同步的参数没有Stream？？？
            hipStatus = hipEventSynchronize(hipEvent_stop);
            if (hipStatus != hipSuccess) {
                fprintf(stderr, "hipEventSynchronize(stop) failed!");
                goto Error;
            }


            std::map<hipStream_t, std::vector<hipEvent_t>>::iterator iter = _start_times.find(stream);
            if (iter != _start_times.end()) { // exist
                hipEvent_t hipEvent_start = (iter->second).back();
                (iter->second).pop_back();

                if ((iter->second).empty()) {
                    _running = false;
                }
                
                // get the elapsed time in this stream
                hipStatus = hipEventElapsedTime(&_elapsed_time, hipEvent_start, hipEvent_stop);
                if (hipStatus != hipSuccess) {
                    fprintf(stderr, "hipEventElapsedTime(&_elapsed_time, start, stop) failed!");
                    goto Error;
                }
                _elapsed_time /= 1000.0;

                // update the splits record
                std::map<const char*, float>::iterator iter_splits = _timer_splits.find(msg);
                if (iter_splits != _timer_splits.end()) {   // exist
                    _timer_splits.at(msg) += _elapsed_time;
                }
                else {
                    _timer_splits.insert(std::map<const char*, float>::value_type(msg, _elapsed_time));
                }
            }
            else {
                fprintf(stderr, "there is no start_event recorded in this stream!");
            }

        }

    Error:

        return;
    }
    
    void recordSplit(const char*& msg) {
        float elapsedTime = getTime();
        // update the splits record
        std::map<const char*, float>::iterator iter_splits = _timer_splits.find(msg);
        if (iter_splits != _timer_splits.end()) {   // exist
            _timer_splits.at(msg) += elapsedTime;
        }
        else {
            _timer_splits.insert(std::map<const char*, float>::value_type(msg, elapsedTime));
        }
    }
    
    void appendSplit(const char*& msg, float time) {
        if (time < 0) {
            fprintf(stderr, "time invalid!");
        }
        else if (_timer_splits.find(msg) != _timer_splits.end()) {
            fprintf(stderr, "record already exist!");
        }
        else {
            _timer_splits.insert(std::map<const char*, float>::value_type(msg, time));
        }
    }
    
    float getSplit(const char*& msg) {
        if (_timer_splits.find(msg) == _timer_splits.end()) {
            return 0.0;
        }
        else {
            return _timer_splits.at(msg);
        }
    }
    
    // destroy the splits recorded
    void clearSplit(const char* &msg) {
        if (_timer_splits.find(msg) != _timer_splits.end()) {
            _timer_splits.erase(msg);
        }
    }
    
    /**
     * @brief Clears all times split messages from the Timer.
     */
    void clearAllSplits() {
        _timer_splits.clear();
    }
    
    void printAllSplits() {
        for (std::map<const char*, float>::iterator iter = _timer_splits.begin(); iter != _timer_splits.end(); iter++) {
            printf("msg: %s , elapsed time: %f seconds\n", iter->first, iter->second);
        }
    }

    /**
     * @brief Returns the time elapsed from startTimer() to stopTimer().
     * @return the elapsed time in seconds
     */
    float getTime() {
        return _elapsed_time;
    }

};

} /* namespace antmoc */




