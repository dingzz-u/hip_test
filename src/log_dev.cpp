#ifndef HIP_RUNTIME_H_
#define HIP_RUNTIME_H_
#include "hip/hip_runtime.h"
#endif

// #include "log_dev.h"

#include <stdio.h>
#include <unistd.h>

namespace antmoc {
  namespace log_dev {

#define MSG_BUFFER_SIZE 100
#define MSG_PREFIX_SIZE 15

	enum LOGLEVEL_DEV {
		/**< A debugging message */
		l_debug,
		/**< A profiling message */
		l_profile,
		/**< A profiling message printed by rank 0 process only */
		l_profile_once,
		/**< An informational but verbose message */
		l_verbose,
		/**< An informational verbose message printed by rank 0 process only */
		l_verbose_once,
		/**< A brief progress update on run progress */
		l_info,
		/**< A brief progress update by node on run progress */
		l_node,
		/**< A message of a single line of characters */
		l_separator,
		/**< A message centered within a line of characters */
		l_header,
		/**< A message sandwiched between two lines of characters */
		l_title,
		/**< A message to warn the user */
		l_warn,
		/**< A message to warn the user - to be printed by rank 0 process only */
		l_warn_once,
		/**< A message to warn of critical program conditions */
		l_critical,
		/**< A message containing program results */
		l_result,
		/**< A messsage for testing */
		l_test,
		/**< A message reporting error conditions */
		l_error
	};
       
        __shared__ char msg_buffer[MSG_BUFFER_SIZE];
        __shared__ char msg_prefix[MSG_PREFIX_SIZE];
        __shared__ LOGLEVEL_DEV _log_level;

		
	__host__ void initialize_host() {
            hipMalloc((void**)_log_level, sizeof(LOGLEVEL_DEV));
	    hipMalloc((void**)&msg_buffer, MSG_BUFFER_SIZE * sizeof(char));
            hipMalloc((void**)&msg_prefix, MSG_PREFIX_SIZE * sizeof(char));
	}
        
        __host__ void destroy(){
        	hipFree(msg_buffer);
		hipFree(msg_prefix);
        }

        // return: position b_i of "\0" in msg_buffer
        __device__ int strcat_buffer(int b_i, char* cat_str) {
        	int i = 0;
        	if(b_i > 0)
			i = b_i;
			int p = 0;
			for (p=0; ; i++,p++) {
            	if(i >= MSG_BUFFER_SIZE){
                	printf("strcat failed because inadequate buffer!\n");
                        i = 0;
                        break;
                }
		msg_buffer[i] = cat_str[p];
                if(cat_str[p] == '\0')
                	break;
		}
                return i;
	}

		__device__ int strcmp_dev(const char* str1, const char* str2) {
			int i = 0;
			for (i = 0; str1[i] != '\0' && str2[i] != '\0'; i++) {
				if (str1[i] < str2[i])return -1;
				if (str1[i] > str2[i])return 1;
				i++;
			}
			if (str1[i] == '\0' && str2[i] == '\0')
				return 0;
			else if (str1[i] != '\0')
				return 1;
			else
				return -1;
		}

		__device__ int get_str_length(const char* str) {
			int length = 0;
			while (str[length] != '\0') {
				length++;
			}
			return length;
		}

		__device__ char* get_capital_str(const char* str) {
			char* capital_s = const_cast<char*>(str);
			for (int i = 0; capital_s[i] != '\0'; i++) {
				if (capital_s[i] >= 'a' && capital_s[i] <= 'z')
					capital_s[i] -= 32;
			}
			return capital_s;
		}

		// 根据char*获得对应的LOGLEVEL
		__device__ LOGLEVEL_DEV getLogLevel(const char*& level_s) {
			if (strcmp_dev(level_s, "debug") == 0) {
				return LOGLEVEL_DEV::l_debug;
			}
			else if (strcmp_dev(level_s, "profile") == 0) {
				return LOGLEVEL_DEV::l_profile;
			}
			else if (strcmp_dev(level_s, "profile_once") == 0) {
				return LOGLEVEL_DEV::l_profile_once;
			}
			else if (strcmp_dev(level_s, "verbose") == 0) {
				return LOGLEVEL_DEV::l_verbose;
			}
			else if (strcmp_dev(level_s, "verbose_once") == 0) {
				return LOGLEVEL_DEV::l_verbose_once;
			}
			else if (strcmp_dev(level_s, "info") == 0) {
				return LOGLEVEL_DEV::l_info;
			}
			else if (strcmp_dev(level_s, "node") == 0) {
				return LOGLEVEL_DEV::l_node;
			}
			else if (strcmp_dev(level_s, "separator") == 0) {
				return LOGLEVEL_DEV::l_separator;
			}
			else if (strcmp_dev(level_s, "header") == 0) {
				return LOGLEVEL_DEV::l_header;
			}
			else if (strcmp_dev(level_s, "title") == 0) {
				return LOGLEVEL_DEV::l_title;
			}
			else if (strcmp_dev(level_s, "warn") == 0) {
				return LOGLEVEL_DEV::l_warn;
			}
			else if (strcmp_dev(level_s, "warn_once") == 0) {
				return LOGLEVEL_DEV::l_warn_once;
			}
			else if (strcmp_dev(level_s, "critical") == 0) {
				return LOGLEVEL_DEV::l_critical;
			}
			else if (strcmp_dev(level_s, "result") == 0) {
				return LOGLEVEL_DEV::l_result;
			}
			else if (strcmp_dev(level_s, "test") == 0) {
				return LOGLEVEL_DEV::l_test;
			}
			else {
				return LOGLEVEL_DEV::l_error;
			}
		}

		__device__ char* getLogLevelStr(LOGLEVEL_DEV log_level) {
			switch (log_level) {
			case LOGLEVEL_DEV::l_debug:
				return "debug";
				break;
			case LOGLEVEL_DEV::l_profile:
				return "profile";
				break;
			case LOGLEVEL_DEV::l_profile_once:
				return "profile_once";
				break;
			case LOGLEVEL_DEV::l_verbose:
				return "verbose";
				break;
			case LOGLEVEL_DEV::l_verbose_once:
				return "verbose_once";
				break;
			case LOGLEVEL_DEV::l_info:
				return "info";
				break;
			case LOGLEVEL_DEV::l_node:
				return "node";
				break;
			case LOGLEVEL_DEV::l_separator:
				return "separator";
				break;
			case LOGLEVEL_DEV::l_header:
				return "header";
				break;
			case LOGLEVEL_DEV::l_title:
				return "title";
				break;
			case LOGLEVEL_DEV::l_warn:
				return "warn";
				break;
			case LOGLEVEL_DEV::l_warn_once:
				return "warn_once";
				break;
			case LOGLEVEL_DEV::l_critical:
				return "critical";
				break;
			case LOGLEVEL_DEV::l_result:
				return "result";
				break;
			case LOGLEVEL_DEV::l_test:
				return "test";
				break;
			case LOGLEVEL_DEV::l_error:
				return "error";
				break;
			default:
				return "error";
				break;
			}
		}

		// return level
		__device__ LOGLEVEL_DEV get_level() {
			return _log_level;
		}

		__device__ void getLevelPrefix(LOGLEVEL_DEV log_level) {
			
			LOGLEVEL_DEV print_level = log_level;
			if (log_level == LOGLEVEL_DEV::l_profile_once)
				print_level = LOGLEVEL_DEV::l_profile;
			else if (log_level == LOGLEVEL_DEV::l_verbose_once)
				print_level = LOGLEVEL_DEV::l_verbose;
			else if (log_level == LOGLEVEL_DEV::l_warn_once)
				print_level = LOGLEVEL_DEV::l_warn;

			char* level_s = getLogLevelStr(print_level);
			int level_length = get_str_length(level_s);
			if (level_length > 8) {
				printf("level length is too long!\n");
				level_s = "ERROR";
				level_length = get_str_length(level_s);
			}

			int space_left = (8 - level_length) / 2;
			int space_right = 8 - level_length - space_left;

			msg_prefix[0] = '[';
            
			// space on left
			for (int i = 0; i < space_left; i++) {
				msg_prefix[i + 1] = ' ';
			}
			// level str
			for (int i = 0; i < level_length; i++) {
				msg_prefix[i + 1 + space_left] = level_s[i];
			}
			// space on right
			for (int i = 0; i < space_right; i++) {
				msg_prefix[i + 1 + space_left + level_length] = ' ';
			}
			msg_prefix[9] = ']';
            msg_prefix[10] = '\0';
		}
		
		// print the log prefix
		__device__ void create_log_prefix(LOGLEVEL_DEV log_level){
			
			getLevelPrefix(log_level);
			
			int x = blockDim.x * blockIdx.x + threadIdx.x;
            int y = blockDim.y * blockIdx.y + threadIdx.y;

			printf("%s [DEV ID: %d,%d] ",msg_prefix, x, y);
		}
		
		__device__ void print1parameters(const char* msg, void* p1){
			int start = 0;		
			int end = 0;
			int find_count = 0;
			char ph[20] = "";
			int i = 0;
			
			for(i=0;msg[i]!='\0';i++){
				if(msg[i]=='%' && msg[i+1]!='\0'){
					switch(msg[i+1]){
						case 'd':{	// %d
							find_count++;
							end = i+1;
							printf("%-*.*s", i, i, msg);
							printf("%d", p1);
							start = i+2;
							i = i+2;
							break;
						}
						case 's':{
                                                        find_count++;
                                                        end = i+1;
                                                        printf("%-*.*s", i, i, msg);
                                                        char* tmp_msg = (char*)p1;
                                                        printf("%s", tmp_msg);
                                                        start = i+2;
                                                        i = i+2;
                                                        break;
                                                }
						case 'f': case 'e':{	// %f
							find_count++;
							end = i+1;
							printf("%-*.*s", i, i, msg);

							if(msg[i+1]=='f')
								printf("%f", *(double *)p1);
							else
								printf("%e", *(double *)p1);

							start = i+2;
							i = i+2;
							break;
						}
						case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':{
							end = i+1;
							while(msg[end]>='0'&&msg[end]<='9'){
								end++;
							}
							if(msg[end]=='f' || msg[end]=='e'){	// %4f
								find_count++;
								printf("%-*.*s", i, i, msg);
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								printf(ph, *(double*)p1);
								start = end+1;
								i = end+1;
								break;
							}else if(msg[end] == '.'){
								if(msg[end+1]>='0'&&msg[end+1]<='9'){
									end++;
									while(msg[end]>='0'&&msg[end]<='9'){
										end++;
									}
									if(msg[end]=='f' || msg[end]=='e'){	// %4.4f
										find_count++;
										printf("%-*.*s", i, i, msg);
										int j = i;
										for(;j<=end;j++){
											ph[j-i] = msg[j];
										}
										ph[j-i] = '\0';
										printf(ph, *(double*)p1);
										start = end+1;
										i = end+1;
										break;
									}
								}
							}
						}
					}
					
					if(find_count > 0){
						break;
					}
				}
			}
			
			if(msg[i]!='\0'){
				const char* latter_msg = &msg[i];
				printf("%s", latter_msg);
			}

			printf("\n");
		}
		
		
		__device__ void print2parameters(const char* msg, void* p1, void* p2){
			
			int start = 0;		
			int end = 0;
			int find_count = 0;
			char ph[20] = "";
			int i = 0;
			
			for(i=0;msg[i]!='\0';i++){
				if(msg[i]=='%' && msg[i+1]!='\0'){
					switch(msg[i+1]){
						case 'd':{	// %d
							find_count++;
							end = i+1;
							
							if( (i-start) > 0){
								const char* former_msg = &msg[start];
								printf("%-*.*s", i-start, i-start, former_msg);
							}
							
							if(find_count == 1)
								printf("%d", p1);
							else if(find_count == 2)
								printf("%d", p2);
							
							start = i+2;
							i = i+2;
							break;
						}
						case 's':{
                                                        find_count++;
                                                        end = i+1;

                                                        if( (i-start) > 0){
                                                                const char* former_msg = &msg[start];
                                                                printf("%-*.*s", i-start, i-start, former_msg);
                                                        }

                                                        if(find_count == 1){
                                                                char* tmp_msg = (char*)p1;
                                                                printf("%s", tmp_msg);
                                                        }else if(find_count == 2){
                                                                char* tmp_msg = (char*)p2;
                                                                printf("%s", tmp_msg);
                                                        }

                                                        start = i+2;
                                                        i = i+2;
                                                        break;
                                                }
						case 'f': case 'e':{	// %f
							find_count++;
							end = i+1;
							
							if( (i-start) > 0){
								const char* former_msg = &msg[start];
								printf("%-*.*s", i-start, i-start, former_msg);
							}
						
							if(msg[i+1] == 'f'){	
								if(find_count == 1)
									printf("%f", *(double *)p1);
								else if(find_count == 2)
									printf("%f", *(double *)p2);
							} else if(msg[i+1] == 'e'){
								if(find_count == 1)
									printf("%e", *(double *)p1);
								else if(find_count == 2)
									printf("%e", *(double *)p2);
							}

							start = i+2;
							i = i+2;
							break;
						}
						case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':{
							end = i+1;
							while(msg[end]>='0'&&msg[end]<='9'){
								end++;
							}
							
							// 4d
							if(msg[end]=='d'){	// %4d
								find_count++;
								
								if( (i-start) > 0){
									const char* former_msg = &msg[start];
									printf("%-*.*s", i-start, i-start, former_msg);
								}
								
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								
								if(find_count == 1)
									printf(ph, p1);
								else if(find_count == 2)
									printf(ph, p2);
								
								start = end+1;
								i = end+1;
								break;
							} else if(msg[end]=='f' || msg[end]=='e'){	// %4f
								find_count++;
								
								if( (i-start) > 0){
									const char* former_msg = &msg[start];
									printf("%-*.*s", i-start, i-start, former_msg);
								}
								
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								
								if(find_count == 1)
									printf(ph, *(double*)p1);
								else if(find_count == 2)
									printf(ph, *(double*)p2);
								
								start = end+1;
								i = end+1;
								break;
							}else if(msg[end] == '.'){
								if(msg[end+1]>='0'&&msg[end+1]<='9'){
									end++;
									while(msg[end]>='0'&&msg[end]<='9'){
										end++;
									}
									if(msg[end]=='f' || msg[end]=='e'){	// %4.4f
										find_count++;
										
										if( (i-start) > 0){
											const char* former_msg = &msg[start];
											printf("%-*.*s", i-start, i-start, former_msg);
										}
										
										int j = i;
										for(;j<=end;j++){
											ph[j-i] = msg[j];
										}
										ph[j-i] = '\0';
										
										if(find_count == 1)
											printf(ph, *(double*)p1);
										else if(find_count == 2)
											printf(ph, *(double*)p2);
										
										start = end+1;
										i = end+1;
										break;
									}
								} else if(msg[end+1] == '*'){
									if(msg[end+2]=='f' || msg[end+2]=='e'){	// %4.*f
										find_count++;
										
										if( (i-start) > 0){
											const char* former_msg = &msg[start];
											printf("%-*.*s", i-start, i-start, former_msg);
										}
										
										end = end+2;
										int j = i;
										for(;j<=end;j++){
											ph[j-i] = msg[j];
										}
										ph[j-i] = '\0';
										
										if(find_count == 1)
											printf(ph, p1, *(double*)p2);
										
										start = end+1;
										i = end+1;
										
										find_count++;
										
										break;
									}
								}
							}
						}
						case '*':{
							end = i+1;
							if(msg[end+1]=='d'){	// %*d
								find_count++;
								end = end+1;
								
								if( (i-start) > 0){
									const char* former_msg = &msg[start];
									printf("%-*.*s", i-start, i-start, former_msg);
								}
								
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								
								if(find_count == 1)
									printf(ph, p1, p2);
								
								start = end+1;
								i = end+1;
								
								find_count++;
								
								break;
							} else if(msg[end+1]=='f' || msg[end+1]=='e'){	// %*f
								find_count++;
								end = end+1;
								
								if( (i-start) > 0){
									const char* former_msg = &msg[start];
									printf("%-*.*s", i-start, i-start, former_msg);
								}
								
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								
								if(find_count == 1)
									printf(ph, p1, *(double*)p1);
								
								start = end+1;
								i = end+1;
								
								find_count++;
								
								break;
							}else if(msg[end+1]=='.'){
								end = end+1;
								if(msg[end]>='0'&&msg[end]<='9'){
									while(msg[end]>='0'&&msg[end]<='9'){
										end++;
									}
									if(msg[end]=='f' || msg[end]=='e'){	// %*.4f
										find_count++;
										
										if( (i-start) > 0){
											const char* former_msg = &msg[start];
											printf("%-*.*s", i-start, i-start, former_msg);
										}
										
										int j = i;
										for(;j<=end;j++){
											ph[j-i] = msg[j];
										}
										ph[j-i] = '\0';
										
										if(find_count == 1)
											printf(ph, p1, *(double*)p2);
										
										start = end+1;
										i = end+1;
										
										find_count++;
										
										break;
									}
								}
							}
						}
					}
					
					if(find_count > 1){
						break;
					}
				}
			}
			
			if(msg[i]!='\0'){
				const char* latter_msg = &msg[i];
				printf("%s", latter_msg);
			}

			printf("\n");
		}
		
		
		__device__ void print3parameters(const char* msg, void* p1, void* p2, void* p3){
			
			int start = 0;		
			int end = 0;
			int find_count = 0;
			char ph[20] = "";
			int i = 0;
			
			for(i=0;msg[i]!='\0';i++){
				if(msg[i]=='%' && msg[i+1]!='\0'){
					switch(msg[i+1]){
						case 'd':{	// %d
							find_count++;
							end = i+1;
							
							if( (i-start) > 0){
								const char* former_msg = &msg[start];
								printf("%-*.*s", i-start, i-start, former_msg);
							}
							
							if(find_count == 1)
								printf("%d", p1);
							else if(find_count == 2)
								printf("%d", p2);
							else if(find_count == 3)
								printf("%d", p3);
							
							start = i+2;
							i = i+2;
							break;
						}
						case 's':{
                                                        find_count++;
                                                        end = i+1;

                                                        if( (i-start) > 0){
                                                                const char* former_msg = &msg[start];
                                                                printf("%-*.*s", i-start, i-start, former_msg);
                                                        }

                                                        if(find_count == 1){
                                                                char* tmp_msg = (char*)p1;
                                                                printf("%s", tmp_msg);
                                                        }else if(find_count == 2){
                                                                char* tmp_msg = (char*)p2;
                                                                printf("%s", tmp_msg);
                                                        }else if(find_count == 3){
                                                                char* tmp_msg = (char*)p3;
                                                                printf("%s", tmp_msg);
                                                        }

                                                        start = i+2;
                                                        i = i+2;
                                                        break;
                                                }
						case 'f': case 'e':{	// %f
							find_count++;
							end = i+1;
							
							if( (i-start) > 0){
								const char* former_msg = &msg[start];
								printf("%-*.*s", i-start, i-start, former_msg);
							}

							if(msg[i+1] == 'f'){
                                                                if(find_count == 1)
                                                                        printf("%f", *(double *)p1);
                                                                else if(find_count == 2)
                                                                        printf("%f", *(double *)p2);
								else if(find_count == 3)
                                                                        printf("%f", *(double *)p3);
                                                        } else if(msg[i+1] == 'e'){
                                                                if(find_count == 1)
                                                                        printf("%e", *(double *)p1);
                                                                else if(find_count == 2)
                                                                        printf("%e", *(double *)p2);
								else if(find_count == 3)
                                                                        printf("%e", *(double *)p3);
                                                        }
							
							start = i+2;
							i = i+2;
							break;
						}
						case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':{
							end = i+1;
							while(msg[end]>='0'&&msg[end]<='9'){
								end++;
							}
							
							// 4d
							if(msg[end]=='d'){	// %4d
								find_count++;
								
								if( (i-start) > 0){
									const char* former_msg = &msg[start];
									printf("%-*.*s", i-start, i-start, former_msg);
								}
								
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								
								if(find_count == 1)
									printf(ph, p1);
								else if(find_count == 2)
									printf(ph, p2);
								else if(find_count == 3)
									printf(ph, p3);
								
								start = end+1;
								i = end+1;
								break;
							} else if(msg[end]=='f' || msg[end]=='e'){	// %4f
								find_count++;
								
								if( (i-start) > 0){
									const char* former_msg = &msg[start];
									printf("%-*.*s", i-start, i-start, former_msg);
								}
								
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								
								if(find_count == 1)
									printf(ph, *(double*)p1);
								else if(find_count == 2)
									printf(ph, *(double*)p2);
								else if(find_count == 3)
									printf(ph, *(double*)p3);
								
								start = end+1;
								i = end+1;
								break;
							}else if(msg[end] == '.'){
								if(msg[end+1]>='0'&&msg[end+1]<='9'){
									end++;
									while(msg[end]>='0'&&msg[end]<='9'){
										end++;
									}
									if(msg[end]=='f' || msg[end]=='e'){	// %4.4f
										find_count++;
										
										if( (i-start) > 0){
											const char* former_msg = &msg[start];
											printf("%-*.*s", i-start, i-start, former_msg);
										}
										
										int j = i;
										for(;j<=end;j++){
											ph[j-i] = msg[j];
										}
										ph[j-i] = '\0';
										
										if(find_count == 1)
											printf(ph, *(double*)p1);
										else if(find_count == 2)
											printf(ph, *(double*)p2);
										else if(find_count == 3)
											printf(ph, *(double*)p3);
										
										start = end+1;
										i = end+1;
										break;
									}
								} else if(msg[end+1] == '*'){
									if(msg[end+2]=='f' || msg[end+2]=='e'){	// %4.*f
										find_count++;
										
										if( (i-start) > 0){
											const char* former_msg = &msg[start];
											printf("%-*.*s", i-start, i-start, former_msg);
										}
										
										end = end+2;
										int j = i;
										for(;j<=end;j++){
											ph[j-i] = msg[j];
										}
										ph[j-i] = '\0';
										
										if(find_count == 1)
											printf(ph, p1, *(double*)p2);
										else if(find_count == 2)
											printf(ph, p2, *(double*)p3);
										
										start = end+1;
										i = end+1;
										
										find_count++;
										
										break;
									}
								}
							}
						}
						case '*':{
							end = i+1;
							if(msg[end+1]=='d'){	// %*d
								find_count++;
								end = end+1;
								
								if( (i-start) > 0){
									const char* former_msg = &msg[start];
									printf("%-*.*s", i-start, i-start, former_msg);
								}
								
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								
								if(find_count == 1)
									printf(ph, p1, p2);
								else if(find_count == 2)
									printf(ph, p2, p3);
								
								start = end+1;
								i = end+1;
								
								find_count++;
								
								break;
							} else if(msg[end+1]=='f' || msg[end+1]=='e'){	// %*f
								find_count++;
								end = end+1;
								
								if( (i-start) > 0){
									const char* former_msg = &msg[start];
									printf("%-*.*s", i-start, i-start, former_msg);
								}
								
								int j = i;
								for(;j<=end;j++){
									ph[j-i] = msg[j];
								}
								ph[j-i] = '\0';
								
								if(find_count == 1)
									printf(ph, p1, *(double*)p1);
								else if(find_count == 2)
									printf(ph, p2, *(double*)p3);
								
								start = end+1;
								i = end+1;
								
								find_count++;
								
								break;
							}else if(msg[end+1]=='.'){
								end = end+1;
								if(msg[end]>='0'&&msg[end]<='9'){
									while(msg[end]>='0'&&msg[end]<='9'){
										end++;
									}
									if(msg[end]=='f' || msg[end]=='e'){	// %*.4f
										find_count++;
										
										if( (i-start) > 0){
											const char* former_msg = &msg[start];
											printf("%-*.*s", i-start, i-start, former_msg);
										}
										
										int j = i;
										for(;j<=end;j++){
											ph[j-i] = msg[j];
										}
										ph[j-i] = '\0';
										
										if(find_count == 1)
											printf(ph, p1, *(double*)p2);
										else if(find_count == 2)
											printf(ph, p2, *(double*)p3);
										
										start = end+1;
										i = end+1;
										
										find_count++;
										
										break;
									}
								}else if(msg[end] == '*'){
									if(msg[end+1]=='f' || msg[end+1]=='e'){	// %*.*f
										end = end+1;
										find_count++;
										
										if( (i-start) > 0){
											const char* former_msg = &msg[start];
											printf("%-*.*s", i-start, i-start, former_msg);
										}
										
										int j = i;
										for(;j<=end;j++){
											ph[j-i] = msg[j];
										}
										ph[j-i] = '\0';
										
										if(find_count == 1)
											printf(ph, p1, p2, *(double*)p3);
										
										start = end+1;
										i = end+1;
										
										find_count++;
										find_count++;
										
										break;
									}
								}
							}
						}
					}
					
					if(find_count > 2){
						break;
					}
				}
			}
			
			if(msg[i]!='\0'){
				const char* latter_msg = &msg[i];
				printf("%s", latter_msg);
			}

			printf("\n");
		}
		
		__device__ void debug(const char* msg, void* p1, void* p2, void* p3) {
			create_log_prefix(LOGLEVEL_DEV::l_debug);
			
			print3parameters(msg, p1, p2, p3);
		}
		
		__device__ void debug(const char* msg, void* p1, void* p2) {
			create_log_prefix(LOGLEVEL_DEV::l_debug);
			
			print2parameters(msg, p1, p2);
		}
		
		
		__device__ void debug(const char* msg, void* p1) {
			create_log_prefix(LOGLEVEL_DEV::l_debug);
			
			print1parameters(msg, p1);
		}

		__device__ void debug(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_debug);
			
			printf("%s\n", msg);
		}

		__device__ void profile(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_profile);
			
			printf("%s\n", msg);
		}

		__device__ void profile_once(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_profile_once);
			
			printf("%s\n", msg);
		}
		
		__device__ void verbose(const char* msg, void* p1, void* p2, void* p3) {
			create_log_prefix(LOGLEVEL_DEV::l_verbose);
			
			print3parameters(msg, p1, p2, p3);
		}
		
		__device__ void verbose(const char* msg, void* p1, void* p2) {
			create_log_prefix(LOGLEVEL_DEV::l_verbose);
			
			print2parameters(msg, p1, p2);
		}
		
		__device__ void verbose(const char* msg, void* p1) {
			create_log_prefix(LOGLEVEL_DEV::l_verbose);
			
			print1parameters(msg, p1);
		}

		__device__ void verbose(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_verbose);
			
			printf("%s\n", msg);
		}
		
		__device__ void verbose_once(const char* msg, void* p1, void* p2, void* p3) {
			int x = blockDim.x * blockIdx.x + threadIdx.x;
            		int y = blockDim.y * blockIdx.y + threadIdx.y;
			if(x==0 && y==0){
				create_log_prefix(LOGLEVEL_DEV::l_verbose_once);
				print3parameters(msg, p1, p2, p3);
			}
		}
		
		__device__ void verbose_once(const char* msg, void* p1, void* p2) {
			int x = blockDim.x * blockIdx.x + threadIdx.x;
                        int y = blockDim.y * blockIdx.y + threadIdx.y;
                        if(x==0 && y==0){
				create_log_prefix(LOGLEVEL_DEV::l_verbose_once);
				print2parameters(msg, p1, p2);
			}
		}
		
		__device__ void verbose_once(const char* msg, void* p1) {
			int x = blockDim.x * blockIdx.x + threadIdx.x;
                        int y = blockDim.y * blockIdx.y + threadIdx.y;
                        if(x==0 && y==0){
				create_log_prefix(LOGLEVEL_DEV::l_verbose_once);
				print1parameters(msg, p1);
			}
		}

		__device__ void verbose_once(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_verbose_once);
			
			printf("%s\n", msg);
		}
		
		__device__ void info(const char* msg, void* p1, void* p2, void* p3) {
			create_log_prefix(LOGLEVEL_DEV::l_info);
			
			print3parameters(msg, p1, p2, p3);
		}
		
		__device__ void info(const char* msg, void* p1, void* p2) {
			create_log_prefix(LOGLEVEL_DEV::l_info);
			
			print2parameters(msg, p1, p2);
		}
		
		__device__ void info(const char* msg, void* p1) {
			create_log_prefix(LOGLEVEL_DEV::l_info);
			
			print1parameters(msg, p1);
		}

		__device__ void info(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_info);
			
			printf("%s\n", msg);
		}
		
		__device__ void info(const char* msg, int d_1, double f_2, double f_3, int d_4, double f_5) {
			create_log_prefix(LOGLEVEL_DEV::l_info);
			
			printf(msg, d_1, f_2, f_3, d_4, f_5);
			printf("\n");
		}
		
		__device__ void info(const char* msg, int d_1, double f_2, int d_3, double f_4, double f_5, double f_6, double f_7, int d_8, double f_9, double f_10, double f_11, int d_12, int d_13, double f_14) {
			create_log_prefix(LOGLEVEL_DEV::l_info);
			
			printf(msg, d_1, f_2, d_3, f_4, f_5, f_6, f_7, d_8, f_9, f_10, f_11, d_12, d_13, f_14);
			printf("\n");
		}

		__device__ void node(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_node);
			
			printf("%s\n", msg);
		}

		__device__ void warn(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_warn);
			
			printf("%s\n", msg);
		}

		__device__ void warn_once(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_warn_once);
			
			printf("%s\n", msg);
		}

		__device__ void critical(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_critical);
			
			printf("%s\n", msg);
		}

		__device__ void result(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_result);
			
			printf("%s\n", msg);
		}

		__device__ void test(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_test);
			
			printf("%s\n", msg);
		}

		__device__ void error(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_error);
			
			printf("%s\n", msg);
		}

		__device__ void separator(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_separator);
			
			printf("%s\n", msg);
		}

		__device__ void header(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_header);
			
			printf("%s\n", msg);
		}

		__device__ void title(const char* msg) {
			create_log_prefix(LOGLEVEL_DEV::l_title);
			
			printf("%s\n", msg);
		}


		__device__ void set_level(LOGLEVEL_DEV new_level) {
			_log_level = new_level;
			
			/*
			char* new_level_s = getLogLevelStr(new_level);
			char* verbose_msg = "Logging level set to %s";
			verbose_once(verbose_msg, new_level_s);
			*/
		}

		__device__ void set_level(const char*& new_level) {
			_log_level = getLogLevel(new_level);

			/*
			char* tmp_msg = "Logging level set to ";
			int b_i = 0;
            b_i = strcat_buffer(0, tmp_msg);
            tmp_msg = const_cast<char*>(new_level);
			b_i = strcat_buffer(b_i, tmp_msg);
            tmp_msg = "\n";
            b_i = strcat_buffer(b_i, tmp_msg);
			verbose_once(msg_buffer);
			*/
		}
	}
}


